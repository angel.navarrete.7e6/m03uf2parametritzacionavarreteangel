﻿/*
* AUTHOR: Angel Navarrete Sanchez
* DATE: 2022/12/14
* DESCRIPTION:  Menu amb conjunt de funcions sobre la parametritzacio
*/
using System;

namespace Metodes
{
    class Metodes
    {
        static void Main(string[] args)
        {
            Metodes inici = new Metodes();
            inici.Menu();
        }
        public void Menu()
        {

            MostrarOpcionsMenu();

            string opcio = DemanarOpcioMenu();
            EscollirOpcioMenu(opcio);
        }
        public void MostrarOpcionsMenu()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                         Exercicis Metodes Angel Navarrete");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.WriteLine("\t1.- RightTriangleSize");
            Console.WriteLine("\t2.- Lamp");
            Console.WriteLine("\t3.- CampsiteOrganizer");
            Console.WriteLine("\t4.- BasicRobot");
            Console.WriteLine("\t5.- ThreeInARow");
            Console.WriteLine("\t6.- SquashCounter");
            Console.WriteLine("\t0.- Sortir");
            Console.WriteLine("------------------------------------------------------------------------------------------------------------------------\n");
            Console.ForegroundColor = ConsoleColor.Gray;
        }
        public string DemanarOpcioMenu()
        {
            Console.Write("Escull una opció: ");
            string opcio = Console.ReadLine();
            return opcio;
        }
        /// <summary>
        /// Funcion para escojer la opción del menú
        /// </summary>
        /// <param name="opcio">Opcion del menu</param>
        public void EscollirOpcioMenu(string opcio)
        {
            do
            {
                switch (opcio)
                {
                    case "1":
                        RightTriangleSize();
                        break;
                    case "2":
                        Lamp();
                        break;
                    case "3":
                        CampsiteOrganizer();
                        break;
                    case "4":
                        BasicRobot();
                        break;
                    case "5":
                        ThreeInARow();
                        break;
                    case "6":
                        SquashCounter();
                        break;
                    case "0":
                        Console.WriteLine("Adeu");
                        break;
                    default:
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Opcio Incorrecta");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        break;
                }
                Console.ReadLine();
                Console.Clear();
                MostrarOpcionsMenu();
                opcio = DemanarOpcioMenu();
            } while (opcio != "0");
        }
        /*DESCRIPTION: Ens digui l'àrea i el perímetre d'una llista de triangles rectangles. */
        public void RightTriangleSize() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------RightTriangleSize--------------------");
            Console.ForegroundColor = ConsoleColor.White;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[Les mesures s'han de posar amb ,]");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write("Introdueix la quantitat de triangles: ");
            int quantitat = Convert.ToInt32(Console.ReadLine());
            double baseTriangle;
            double alturaTriangle;
            for (int i = 1; i <= quantitat; i++)
            {
                Console.Write("Introdueix la base: ");
                baseTriangle = Convert.ToDouble(Console.ReadLine());
                Console.Write("Introdueix l'altura: ");
                alturaTriangle = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Un triangle de " + baseTriangle + " x " + alturaTriangle + " te " + CalcularArea(baseTriangle, alturaTriangle) + " d'area i " + CalcularPerimetre(baseTriangle, alturaTriangle) + " de perimetre.");
            } 
        }
        public double CalcularArea(double baseT, double altura) {
            double area = (baseT * altura) / 2;
            return area;
        
        }
        public double CalcularPerimetre(double baseT, double altura) {
            double perimetre;
            double hipotenusa = Math.Pow(baseT, 2) + Math.Pow(altura, 2);
            perimetre = hipotenusa + baseT + altura;
            return perimetre;
        
        }
        /*DESCRIPTION: Simuli una làmpada, amb les accions:
                        TURN ON: Encén la làmpada
                        TURN OFF: Apaga la làmpada
                        TOGGLE: Canvia l'estat de la làmpada
        */
        public void Lamp() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------Lamp-------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string accio;
            bool lampStatus = false;
            do
            {
                Console.Write("> ");
                accio = Console.ReadLine();
                if (accio.ToUpper() != "END")
                {
                    if (accio.ToUpper() == "TURN ON" || accio.ToUpper() == "TURN OFF" || accio.ToUpper() == "TOGGLE")
                    {
                        lampStatus = ManageLamp(accio, lampStatus);
                        Console.WriteLine(lampStatus);
                    }
                    else { Console.WriteLine("No es una accio de la llum"); }
                }
            } while (accio.ToUpper() != "END");
        }
        public bool ManageLamp(string accio, bool resultat)
        {
            if (accio.ToUpper() == "TURN ON") resultat = true;
            if (accio.ToUpper() == "TURN OFF") resultat = false;
            if (accio.ToUpper() == "TOGGLE") resultat = !resultat;
            return resultat;
        }
        /*DESCRIPTION: Gestionar un camping. Volem tenir controlat quantes parcel·les tenim plenes i quanta gent tenim.*/
        public void CampsiteOrganizer() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------CampsiteOrganizer-------------------");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[El programa nomès aceptarà les comandes ENTRA o MARXA]");
            Console.ForegroundColor = ConsoleColor.White;
            string accio;
            int[] persones = new int[10];
            int parceles = 0;
            string[] reservaPersona = new string[10];
            int totalPersones = 0;
            int cont = 0;
            do
            {
                Console.Write("> ");
                accio = Console.ReadLine();
                if (accio.ToUpper() != "END")
                {
                    if (parceles < 10)
                    {
                        if (accio.ToUpper().Contains("ENTRA"))
                        {
                            persones[cont] = EnterCampsite(accio.ToUpper());
                            reservaPersona[cont] = ReservaPersona(accio.ToUpper());
                            totalPersones += persones[cont];
                            parceles++;
                            cont++;
                            Console.WriteLine("Parceles: " + parceles);
                            Console.WriteLine("Persones: " + totalPersones);
                        }
                        else if (accio.ToUpper().Contains("MARXA"))
                        {
                            totalPersones -= ExitCampsite(accio.ToUpper(), reservaPersona, persones);
                            parceles--;
                            Console.WriteLine("Parceles: " + parceles);
                            Console.WriteLine("Persones: " + totalPersones);
                        }
                        else
                        {
                            Console.WriteLine("Instrucció incorrecte");
                        }
                    }
                    else
                    {
                        Console.WriteLine("El máxim de parceles es 10");
                    }
                }
            } while (accio.ToUpper() != "END");
        }

        public int EnterCampsite(string accio) {
            int persones;
            string[] array = accio.Split(" ");
            persones = Convert.ToInt32(array[1]);
            return persones; 
        }
        public string ReservaPersona(string accio) {
            string[] array = accio.Split(" ");
            string nomPersona = array[2];
            return nomPersona;
        }
        public int ExitCampsite(string accio, string[] noms, int[] numeroPersones) {
            int numeroPersonesSortida = 0;
            string[] array = accio.Split(" ");

            for (int i = 0; i < noms.Length; i++)
            {
                if (noms[i] == array[1])
                {
                    numeroPersonesSortida = numeroPersones[i];
                }
            }
            return numeroPersonesSortida;
        }
        

        /*DESCRIPTION: Permet moure un robot en un planell 2D.
        El robot ha de guardar la seva posició (X, Y) i la velocitat actual. Per defecte, la posició serà (0, 0) i la velocitat és 1. La velocitat indica la distància que recorre el robot en cada acció.
        */
        public void BasicRobot() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------BasicRobot-------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string accio;
            double posicioX = 0.0;
            double posicioY = 0.0;
            double velocitat = 1.0;
            do
            {
                Console.Write("> ");
                accio = Console.ReadLine();
                if (accio.ToUpper() != "END")
                {
                    if (accio.ToUpper() == "DALT" || accio.ToUpper() == "BAIX") posicioY=MouRobotY(accio.ToUpper(),posicioY,velocitat);
                    if (accio.ToUpper() == "ESQUERRA" || accio.ToUpper() == "DRETA") posicioX=MouRobotX(accio.ToUpper(), posicioX, velocitat);
                    if (accio.ToUpper() == "ACELERAR" || accio.ToUpper() == "DISMINUIR") velocitat=CanviaVelocitat(accio.ToUpper(), velocitat);
                    if (accio.ToUpper() == "POSICIO") MostrarPosicio(posicioX,posicioY);
                    if (accio.ToUpper() == "VELOCITAT") MostrarVelocitat(velocitat);

                }
            } while (accio.ToUpper() != "END");
        }
        public double MouRobotY(string accio,double x, double velocitat) {
            if (accio.ToUpper() == "DALT") x += velocitat;
            if (accio.ToUpper() == "BAIX") x -= velocitat;
            return x;
        }
        public double MouRobotX(string accio,double y, double velocitat) {
            if (accio.ToUpper() == "DRETA") y += velocitat;
            if (accio.ToUpper() == "ESQUERRA") y -= velocitat;
            return y;
        }
        public double CanviaVelocitat(string accio,double velocitat) {
            if (velocitat <10.0 && velocitat > 0.0)
            {
                if (accio.ToUpper() == "ACELERAR") velocitat += 0.5;
                if (accio.ToUpper() == "DISMINUIR") velocitat -= 0.5;
            }
            if (velocitat >= 10.0) Console.WriteLine("No es pot modificar perque esta en el seu máxim");
            if (velocitat <= 0.0) Console.WriteLine("No es pot modificar perque esta en el seu mínim");
            return velocitat;
        }
        public void MostrarPosicio(double x,double y) {
            Console.WriteLine("La posicio del robot es ( " + x + ", " + y + " )");
        }
        public void MostrarVelocitat(double velocitat) {
            Console.WriteLine("La velocitat del robot es ( "+ velocitat + " )");
        }

        /*DESCRIPTION: Es juga amb un taulell de 3x3. Cada jugador col·loca una peça al taulell alternativament. Guanya qui és capaç de col·locar-les formant una línia de 3, ja sigui vertical, horitzontal o diagonal.*/
        public void ThreeInARow() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------ThreeInARow-------------------");
            Console.ForegroundColor = ConsoleColor.White;
            string[,] tauler = { { "-", "-", "-"}, { "-", "-", "-" }, { "-", "-", "-" }};
            bool partidaAcabada = false;
            bool tresEnRatlla = false;
            bool empat = false;
            int x;
            int y;

            do
            {

                for (int i = 0; i < tauler.Length; i++)
                {
                    if (i % 2 == 1)
                    {
                        if ((tresEnRatlla == true) || (empat == true)) partidaAcabada = true;
                        else if ((tresEnRatlla == false) || (empat == false))
                        {
                            Console.WriteLine("Juga les X");
                            Console.WriteLine("Introdueix les coordenades on vols posar la fitxa");
                            x = DemanaX();
                            y = DemanaY();
                            Jugada(tauler, x, y, "X");
                            ImprimirMatriu(tauler);
                            tresEnRatlla = ComprovarJugada(tauler, tresEnRatlla);
                            empat = ComprovarEmpat(tauler);
                            if (empat == true)
                            {
                                Console.WriteLine("Empat Tecnic");
                                tresEnRatlla = true;
                            }
                        }
                    }
                    else if (i % 2 == 0)
                    {
                        if ((tresEnRatlla == true) || (empat == true)) partidaAcabada = true;
                        else if ((tresEnRatlla == false) || (empat == false))
                        {
                            Console.WriteLine("Juga les 0");
                            Console.WriteLine("Introdueix les coordenades on vols posar la fitxa");
                            x = DemanaX();
                            y = DemanaY();
                            Jugada(tauler, x, y, "0");
                            ImprimirMatriu(tauler);
                            tresEnRatlla = ComprovarJugada(tauler, tresEnRatlla);
                            empat = ComprovarEmpat(tauler);
                            if (empat == true)
                            {
                                Console.WriteLine("Empat Tecnic");
                                tresEnRatlla = true;
                            }
                        }
                    }
                }

            } while (partidaAcabada == false);
        }


        public int DemanaX()
        {
            int x = Convert.ToInt32(Console.ReadLine());
            return x;
        }
        public int DemanaY()
        {
            int y = Convert.ToInt32(Console.ReadLine());
            return y;
        }

        public bool ComprovarJugada(string [,] matriu, bool tresEnRatlla)
        {
            for (int i = 0; i < matriu.GetLength(0); i++)
            {

                if (matriu[i, 0] == "0" && matriu[i, 1] == "0" && matriu[i, 2] == "0")
                {
                    Console.WriteLine("Guanyen les 0");
                    tresEnRatlla= true;
                }
                else if (matriu[i, 0] == "X" && matriu[i, 1] == "X" && matriu[i, 2] == "X")
                {
                    Console.WriteLine("Guanyen les X");
                    tresEnRatlla = true;
                }
                else if (matriu[0, i] == "0" && matriu[1, i] == "0" && matriu[2, i] == "0")
                {
                    Console.WriteLine("Guanyen les 0");
                    tresEnRatlla = true;
                }
                else if (matriu[0, i] == "X" && matriu[1, i] == "X" && matriu[2, i] == "X")
                {
                    Console.WriteLine("Guanyen les X");
                    tresEnRatlla = true;
                }
                else if ((matriu[0,0]== "0" && matriu[1,1]=="0" & matriu[2, 2] == "0") || (matriu[0, 2] == "0" && matriu[1, 1] == "0" & matriu[2, 0] == "0"))
                {
                    Console.WriteLine("Guanyen les 0");
                    tresEnRatlla = true;
                }
                else if ((matriu[0, 0] == "X" && matriu[1, 1] == "X" & matriu[2, 2] == "X") || (matriu[0, 2] == "X" && matriu[1, 1] == "X" & matriu[2, 0] == "X"))
                {
                    Console.WriteLine("Guanyen les X");
                    tresEnRatlla = true;
                }

            }
            return tresEnRatlla;
        }

        public bool ComprovarEmpat(string[,] matriu)
        {
            for (int i = 0; i < matriu.GetLength(0); i++)
            {
                for (int j = 0; j < matriu.GetLength(0); j++)
                {
                    if (matriu[i,j].Contains("-"))
                    {
                        return false;
                    }
                    
                }
            }
            return true;
        }
        public void Jugada(string[,] matriu, int x, int y, string jugador ) {
            bool correcte = false;
            do
            {
                if (matriu[x, y] == "-")
                {
                    matriu[x, y] = jugador;
                    correcte = true;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Espai Ocupat");
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine("Torna a escriure un altre posició");
                    x = DemanaX();
                    y = DemanaY();

                }
            } while (correcte == false);
            
        }

        public void ImprimirMatriu(string[,] matriu) {
            int i = 0;
            Console.WriteLine("Tauler de 3 en ratlla");
            foreach (var element in matriu)
            {
                Console.Write(element);
                if (i < matriu.Length - 1)
                {
                    Console.Write(" ");
                    i++;
                }
                if (i == matriu.GetLength(0))
                {
                    Console.WriteLine();
                    i = 0;
                }
            }
        }

        /*DESCRIPTION: Volem fer un marcador per un partit de squash
         L'usuari primer introduirà el número de partits a introduir.
         De cada partit, escriurà qui guanya cada punt i finalitzarà amb una F.
        */
        public void SquashCounter() {
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("--------------------SquashCounter-------------------");
            Console.ForegroundColor = ConsoleColor.White;

            Console.WriteLine("Introdueix la quantitat de sets: ");
            int sets = Convert.ToInt32(Console.ReadLine());

            bool GuanyadorSquash = false;
            int contadorJugador1 = 0;
            int contadorJugador2 = 0;
            string jugada;
            do
            {
                if ((contadorJugador1 < 3) || (contadorJugador2 < 3))
                {
                    for (int i = 0; i < sets; i++)
                    {
                        if (contadorJugador1 == 3)
                        {
                            Console.WriteLine("Ha guanyat el Jugador A");
                            GuanyadorSquash = true;
                        }
                        else if (contadorJugador2 == 3)
                        {
                            Console.WriteLine("Ha guanyat el Jugador B");
                            GuanyadorSquash = true;
                        }
                        if (GuanyadorSquash == false)
                        {
                            jugada = DemanarJugada();
                            ImprimirJugada(jugada);
                            if (ComprovarJugadaSquash(jugada) == 1)
                            {
                                contadorJugador1++;
                            }
                            else if (ComprovarJugadaSquash(jugada) == 2)
                            {
                                contadorJugador2++;
                            }
                        }
                    }
                }
                
            } while (GuanyadorSquash == false);
            
        }

        public string DemanarJugada() {
            Console.WriteLine("Indrodueix el marcador del set");
            string jugada = Console.ReadLine();
            if (jugada.ToUpper().LastIndexOf("F") == -1)
            {
                Console.WriteLine("Set no finalitzat, introdueix un altre vegada el marcador");
                jugada = Console.ReadLine();
            }
            else if (jugada.Length < 10)
            {
                Console.WriteLine("Set no finalitzat, introdueix un altre vegada el marcador");
                jugada = Console.ReadLine();
            }
            return jugada.ToUpper();
        }
        public void ImprimirJugada(string jugada)
        {
            int countA = 0;
            int countB = 0;
            for (int i = 0; i < jugada.Length; i++)
            {
                if (jugada[i] == 'A')
                {
                    countA++;
                }
                else if (jugada[i] == 'B')
                {
                    countB++;  
                }
                if (countA == 11 && countA - 2 == countB)
                {
                    Console.WriteLine("11-9");
                }
                else if (countB == 11 && countB - 2 == countA)
                {
                    Console.WriteLine("9-11");
                }
            }
            Console.WriteLine(countA +"-"+countB);
        }
        public int ComprovarJugadaSquash(string jugada) {
            int countA = 0;
            int countB = 0;
            for (int i = 0; i < jugada.Length-1; i++)
            {
                if(jugada[i] == 'A')
                {
                    countA++;
                }
                else if (jugada[i] == 'B')
                {
                    countB++;
                }
            }
            if (countA > countB || countA-2 == countB)
            {
                return 1;
            }
            return 2;
        }
    }
}
